# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2020-09-14 13:08
from __future__ import unicode_literals

import uuid

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models

try:
    from django.db.models import JSONField
except ImportError:
    from django.contrib.postgres.fields.jsonb import JSONField


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('django_audit_events', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArchivedAuditEvent',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False, verbose_name='ID')),
                ('remote_addr', models.GenericIPAddressField(blank=True, null=True, verbose_name='IP address')),
                ('url', models.URLField(blank=True, null=True, verbose_name='URL')),
                ('query_params', JSONField(blank=True, default=dict, null=True, verbose_name='Query params')),
                ('post_data', JSONField(blank=True, default=dict, null=True, verbose_name='Post data')),
                ('object_id', models.PositiveIntegerField()),
                ('content', JSONField(default=dict, verbose_name='Content')),
                ('timestamp', models.DateTimeField(verbose_name='Timestamp')),
                ('archive_timestamp', models.DateTimeField(auto_now_add=True, verbose_name='Archive timestamp')),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
            options={
                'ordering': ('-timestamp',),
                'abstract': False,
                'verbose_name': 'Archived audit event',
                'swappable': 'AUDIT_EVENT_ARCHIVE_MODEL',
                'verbose_name_plural': 'Archived audit events',
            },
        ),
    ]
