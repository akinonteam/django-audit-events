# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2020-12-18 11:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('django_audit_events', '0002_archivedauditevent'),
    ]

    operations = [
        migrations.AlterField(
            model_name='archivedauditevent',
            name='url',
            field=models.URLField(blank=True, max_length=256, null=True, verbose_name='URL'),
        ),
        migrations.AlterField(
            model_name='auditevent',
            name='url',
            field=models.URLField(blank=True, max_length=256, null=True, verbose_name='URL'),
        ),
    ]
