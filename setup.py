import os
import sys

from setuptools import find_packages, setup

# This should always be before importing django_audit_events
# We need working directory in sys.path for setuptools.build_meta
# else we get ImportError
sys.path.insert(0, os.path.dirname(__file__))

from django_audit_events import (  # isort:skip
    __author__,
    __doc__,
    __email__,
    __license__,
    __maintainer__,
)

if sys.version_info[0] == 2:
    from io import open


def get_long_description():
    with open("README.md", "r", encoding="utf-8") as fh:
        return fh.read()


setup(
    name="django-audit-events",
    use_scm_version={
        "write_to": "django_audit_events/version.py",
        "write_to_template": '__version__ = "{version}"\n',
    },
    description=__doc__,
    long_description=get_long_description(),
    long_description_content_type="text/markdown",
    author=__author__,
    author_email=__email__,
    maintainer=__maintainer__,
    maintainer_email=__email__,
    license=__license__,
    url="https://bitbucket.org/akinonteam/django-audit-events",
    project_urls={
        "Documentation": "https://django-audit-events.readthedocs.io/",
        "Source Code": "https://bitbucket.org/akinonteam/django-audit-events",
    },
    packages=find_packages(exclude=["tests", "tests.*"]),
    platforms="any",
    zip_safe=False,
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Web Environment",
        "Framework :: Django",
        "Framework :: Django :: 1.10",
        "Framework :: Django :: 1.11",
        "Framework :: Django :: 2.0",
        "Framework :: Django :: 2.1",
        "Framework :: Django :: 2.2",
        "Framework :: Django :: 3.0",
        "Framework :: Django :: 3.1",
        "Framework :: Django :: 3.2",
        "Framework :: Django :: 4.0",
        "Framework :: Django :: 4.1",
        "Framework :: Django :: 4.2",
        "Framework :: Django :: 5.0",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Programming Language :: Python :: 3.12",
    ],
)
